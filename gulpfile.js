var del = require("del");
var fs = require("fs");
var gulp = require("gulp");
var concat = require("gulp-concat");
var concatCss = require("gulp-concat-css");
var csso = require("gulp-csso");
var stylus = require("gulp-stylus");
var uglify = require("gulp-uglify");
var zip = require("gulp-zip");

gulp.task("dir", function(){
	return gulp.src("*.*", {read: false})
		.pipe(gulp.dest("./dist/build"));
});

gulp.task("stylus", function () {
	return gulp.src(["./src/css/*.styl"])
		.pipe(stylus())
		.pipe(concatCss("main.css"))
		.pipe(csso())
		.pipe(gulp.dest("./dist/build"));
});

gulp.task("js", function(){
	return gulp.src(["./src/js/*.js"])
		.pipe(concat("main.js"))
		.pipe(uglify())
		.pipe(gulp.dest("./dist/build"));
});

gulp.task("build", function(){
	var content = fs.readFileSync("./src/manifest.json", {
		encoding: "utf-8"  
	});
	var vernum = content.match(/\.[0-9]{2,}/g);
	var vernum2 = (parseFloat(vernum) + 0.01).toFixed(2).replace(/^0+/, "");
	fs.writeFileSync("./src/manifest.json", content.replace(vernum, vernum2));
	var fullnum = content.match(/[0-9]\.[0-9]/g);
	return gulp.src([
		"./dist/build/*",
		"./src/icon48.png",
		"./src/icon128.png",
		"./src/manifest.json"
	])
		.pipe(zip("Customized-Steam-Profiles.zip"))
		.pipe(gulp.dest("./dist/"+fullnum+vernum2));
});

gulp.task("clean", function(){
	return del("./dist/build");
});

gulp.task("default", gulp.series("dir", "js", "build", "clean"));