var profileid;

$(function(){
	//Check if URL matches a profile URL
	if (window.location.href.match(/\/id\/|\/profiles\//g)) {
		AJAXProfile();
	}
});


function AJAXProfile(){
	//Set ProfileID
	profileid = $("input[name=\"abuseID\"]").val();
    
	//AJAX Steam.Design API for profile data
	$.ajax({
		url: "https://csp.steam.design/api/"+profileid+".json"
	}).done(function(data){
		loadProfile(data);
	}).error(function(){
		return;
	});
}

function loadProfile(){
	//unimplemented
}